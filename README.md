# Composer template for Drupal projects - Drofile8 Installation Profile

This composer template is based on drupal-project:
[![Build Status](https://travis-ci.org/drupal-composer/drupal-project.svg?branch=8.x)](https://travis-ci.org/drupal-composer/drupal-project)

## Usage

First you need to [install composer](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx).

> Note: The instructions below refer to the [global composer installation](https://getcomposer.org/doc/00-intro.md#globally).
You might need to replace `composer` with `php composer.phar` (or similar) 
for your setup.

After that you can start working on this project:

1. Clone this repository:

    ```
    git clone git@gitlab.com:anairamzap/d8base.git
    ```
2. cd to this repo root and install dependencies:

    ```
    cd d8base && composer install
    ```
4. Create a db, dbuser, grant perms:

    ```
    $ mysql -u root -p

    mysql> create database dbname; GRANT ALL PRIVILEGES ON dbname.* To 'username'@'host' IDENTIFIED BY 'superstrongpassword'; FLUSH PRIVILEGES;
    ```
3. Install Drupal. 
    You can choose between: 
    - Using the webinstaller: Configure your webserver to point to the `web` directory, in a browser go to http://localhost/install.php and choose **drofile8** as profile.
    - or using drush: 

      ```
      drush si drofile8 \
      --db-url=mysql://username:superstrongpassword@localhost/dbname \
      --account-mail="dp_admin@domain.com" \
      --account-name=dp_admin \
      --account-pass=NobodywouldGues5! \
      --site-mail="dp_admin@domain.com" \
      --site-name="Drupal 8 basic site" 
      ```

4. Your new Drupal 8 site is ready! If everything went well, you should have a brand new Drupal 8 site, already configured with some basics (modules, a base theme, etc...)

### If only I had some time!

When I manage to have some spare time I would like to improve this profile, with the following:

- [ ] Add Travis to have project status
- [ ] Get a lovely frontend dev to take a look at the theme and add some more fancy stuff in it.
- [ ] Enhance these docs :)
- [ ] Docker? mhh... for those who don't like local dev ;)

