(function ($) {

    var swipeboxInit = function(){
        // Swipebox
        // For full settings see http://brutaldesign.github.io/swipebox/#options
        // Usage:
        /**
         *  <a class="swipebox" href="*target*" target="_blank"></a>
         */

    	var swipeBox = $('.swipebox');

        if (swipeBox.length > 0) {
            $(document).swipebox({ selector: '.swipebox' });
        }
    };

    // Document.ready
    $(function() {
        swipeboxInit();
    });

})(jQuery);