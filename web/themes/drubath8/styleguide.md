# Drubath Style Guide

## Table of Contents

  1. [Terminology](#terminology)
    - [Rule Declaration](#rule-declaration)
    - [Selectors](#selectors)
    - [Properties](#properties)
  1. [SASS](#sass)
    - [Formatting](#formatting)
    - [Comments](#comments)
    - [ID Selectors](#id-selectors)
    - [Border](#border)
    - [Syntax](#syntax)
    - [Ordering](#ordering-of-property-declarations)
    - [Variables](#variables)
    - [Mixins](#mixins)
    - [Nested selectors](#nested-selectors)
    - [Media queries](#media-queries)
  1. [Javascript](#javascript)
  1. [BEM](#bem)

## Terminology

### Rule declaration

A “rule declaration” is the name given to a selector (or a group of selectors) with an accompanying group of properties. Here's an example:

```css
.listing {
  font-size: 18px;
  line-height: 1.2;
}
```

### Selectors

In a rule declaration, “selectors” are the bits that determine which elements in the DOM tree will be styled by the defined properties. Selectors can match HTML elements, as well as an element's class, ID, or any of its attributes. Here are some examples of selectors:

```css
.my-element-class {
  /* ... */
}

[aria-hidden] {
  /* ... */
}
```

### Properties

Finally, properties are what give the selected elements of a rule declaration their style. Properties are key-value pairs, and a rule declaration can contain one or more property declarations. Property declarations look like this:

```css
/* some selector */ {
  background: #f1f1f1;
  color: #333;
}
```

## SASS

### Formatting

* Use tab (4 spaces) for indentation
* Prefer dashes over camelCasing in class names.
* Do not use ID selectors
* When using multiple selectors in a rule declaration, give each selector its own line.
* Put a space before the opening brace `{` in rule declarations
* In properties, put a space after, but not before, the `:` character.
* Put closing braces `}` of rule declarations on a new line
* Put blank lines between rule declarations
* Always use single quotes
* Always include quotes around background image urls (i.e. `background-image:url('../images/logo.png');`)

**Bad**

```css
.avatar{
    border-radius:50%;
    border:2px solid white; }
.no, .nope, .not_good {
    // ...
}
#lol-no {
  // ...
}
```

**Good**

```css
.avatar {
  border-radius: 50%;
  border: 2px solid white;
}

.one,
.selector,
.per-line {
  // ...
}
```

### Comments

* Use line comments (`//`) to write comments.
* Prefer comments on their own line. Avoid end-of-line or inline comments.
* Write detailed comments for code that isn't self-documenting:
  - Uses of z-index
  - Compatibility or browser-specific hacks


### ID selectors

While it is possible to select elements by ID in CSS, it should generally be considered an anti-pattern. ID selectors introduce an unnecessarily high level of [specificity](https://developer.mozilla.org/en-US/docs/Web/CSS/Specificity) to your rule declarations, and they are not reusable.

For more on this subject, read [CSS Wizardry's article](http://csswizardry.com/2014/07/hacks-for-dealing-with-specificity/) on dealing with specificity.

### Border

Use `0` instead of `none` to specify that a style has no border.

**Bad**

```css
.foo {
    border: none;
}
```

**Good**

```css
.foo {
    border: 0;
}
```

### Syntax

* Use the `.scss` syntax, never the original `.sass` syntax
* Order your regular CSS and `@include` declarations logically (see below)

### Ordering of property declarations

1. Property declarations

    List all standard property declarations, anything that isn't an `@include` or a nested selector.

    ```scss
    .btn-green {
        background: green;
        font-weight: bold;
        // ...
    }
    ```

2. `@include` declarations go first (except `@include media()`)

    Grouping `@include`s at the beginning makes it easier to read the entire selector and override any properties declared by the mixin.
    Place `@include media()` at the end.

    ```scss
    .btn-green {
        @include transition(background 0.5s ease);
        background: green;
        font-weight: bold;
        
        @include media('<=small'){
            font-weight:normal;
        }
        // ...
    }
    ```

3. Pseudo selectors
    Pseudo selectors (e.g. `:before`, `:hover`) go next after the parent element's styling

    ```scss
    .btn {
        @include transition(background 0.5s ease);
        background: green;
        font-weight: bold;
        
        &:before {
            content:' ';
        }
    }
    ```

4. Nested selectors

    Nested selectors, _if necessary_, go last, and nothing goes after them. Add whitespace between your rule declarations and nested selectors, as well as between adjacent nested selectors. Apply the same guidelines as above to your nested selectors.

    ```scss
    .btn {
        @include transition(background 0.5s ease);
        background: green;
        font-weight: bold;
        
        &:before {
            content: ' ';
        }

        .icon {
            margin-right: 10px;
        }
    }
    ```

### Variables

Prefer dash-cased variable names (e.g. `$my-variable`) over camelCased or snake_cased variable names. It is acceptable to prefix variable names that are intended to be used only within the same file with an underscore (e.g. `$_my-variable`).

### Mixins

Mixins should be used to DRY up your code, add clarity, or abstract complexity--in much the same way as well-named functions. Mixins that accept no arguments can be useful for this, but note that if you are not compressing your payload (e.g. gzip), this may contribute to unnecessary code duplication in the resulting styles.

### Nested selectors

Try where possible to avoid selectors being nested more than 3 levels deep. Drupal can make this problematic, so avoid where possible.

```scss
.page-container {
  .content {
    .profile {
      // STOP!
    }
  }
}
```

When selectors become this long, you're likely writing CSS that is:

* Strongly coupled to the HTML (fragile) *—OR—*
* Overly specific (powerful) *—OR—*
* Not reusable


Again: **never nest ID selectors!**

If you must use an ID selector in the first place (and you should really try not to), they should never be nested. If you find yourself doing this, you need to revisit your markup, or figure out why such strong specificity is needed. If you are writing well formed HTML and CSS, you should **never** need to do this.

### Media queries

Using the included `@import media()` mixin (See `_setup.scss` for default breakpoints), media queries should be placed inline with their parent styling, rather than at the end of the document.

## Javascript
The only use case for custom IDs (i.e. any IDs that aren't added by Drupal), would be for targeted use in javascript. They should be prepended with `js-` and use camelCase, e.g:
```html
<div id="js-homeCarousel" class="carousel">
    <div class="carousel__item"></div>
</div>
```

## BEM

Where relevant, classes should follow the BEM standard for naming conventions ([getbem.com](http://getbem.com/)), e.g:

```html
<div class="card">
    <h2 class="card__title">title</h2>
    <p>Lorem ipsum</p>
    <a href="card__link">link</a>
    <a href="card__link card__link--special">special link</a>
</div>
```